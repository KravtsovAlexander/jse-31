package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
