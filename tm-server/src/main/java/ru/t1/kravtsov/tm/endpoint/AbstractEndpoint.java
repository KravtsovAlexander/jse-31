package ru.t1.kravtsov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.service.IServiceLocator;
import ru.t1.kravtsov.tm.dto.request.AbstractUserRequest;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.exception.system.AccessDeniedException;
import ru.t1.kravtsov.tm.model.Session;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final Session session = serviceLocator.getAuthService().validateToken(token);
        @Nullable final Role sessionRole = session.getRole();
        if (sessionRole == null || !sessionRole.equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}
