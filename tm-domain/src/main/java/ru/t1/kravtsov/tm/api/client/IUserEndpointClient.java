package ru.t1.kravtsov.tm.api.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.endpoint.IConnectionProvider;
import ru.t1.kravtsov.tm.api.endpoint.IUserEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IUserEndpointClient extends IEndpointClient, IUserEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpointClient.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpointClient.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

}
