package ru.t1.kravtsov.tm.exception.system;

public final class AuthException extends AbstractSystemException {

    public AuthException() {
        super("Error. Not authenticated.");
    }

}
