package ru.t1.kravtsov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Project;

@NoArgsConstructor
public class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(final @Nullable Project project) {
        super(project);
    }

}
