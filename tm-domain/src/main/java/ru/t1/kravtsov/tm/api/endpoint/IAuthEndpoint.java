package ru.t1.kravtsov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.UserLoginRequest;
import ru.t1.kravtsov.tm.dto.request.UserLogoutRequest;
import ru.t1.kravtsov.tm.dto.request.UserViewProfileRequest;
import ru.t1.kravtsov.tm.dto.response.UserLoginResponse;
import ru.t1.kravtsov.tm.dto.response.UserLogoutResponse;
import ru.t1.kravtsov.tm.dto.response.UserViewProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    );

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    UserViewProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    );

}
