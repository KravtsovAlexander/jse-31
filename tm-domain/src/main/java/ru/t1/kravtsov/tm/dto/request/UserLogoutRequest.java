package ru.t1.kravtsov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(final @Nullable String token) {
        super(token);
    }

}
