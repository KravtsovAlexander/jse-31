package ru.t1.kravtsov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.DataYamlSaveFasterXmlRequest;
import ru.t1.kravtsov.tm.enumerated.Role;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data to yaml file";

    @NotNull
    public static final String NAME = "data-save-yaml-fasterxml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA YAML SAVE]");
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
