package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.ProjectRemoveByNameRequest;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove project by name.";

    @NotNull
    public static final String NAME = "project-remove-by-name";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByNameRequest request = new ProjectRemoveByNameRequest(getToken());
        request.setName(name);
        getProjectEndpoint().removeProjectByName(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
