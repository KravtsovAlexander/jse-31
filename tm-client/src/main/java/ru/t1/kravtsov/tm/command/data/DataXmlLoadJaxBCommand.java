package ru.t1.kravtsov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.kravtsov.tm.enumerated.Role;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file";

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA XML LOAD]");
        getDomainEndpoint().loadDataXmlJaxB(new DataXmlLoadJaxBRequest());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
